<?php
// Wersja FLEKSa
$wersja = '2.2.2';

// Współrzędne geograficzne.
// Domyślnie z dane.php
$geo_lat = $szerokosc_geograficzna;
$geo_lng = $dlugosc_geograficzna;

// Jeśli istnieje cookie od obu współrzędnych - podmienia współrzędne na użytkownika.
if (isset($_COOKIE['geo_lng']) && isset($_COOKIE['geo_lat']))
{
	// Długość geograficzna: od -180 do 180 stopni 
	$geo_lng = intval($_COOKIE['geo_lng']); 
	// Dodatkowy safecheck
	if (is_numeric($geo_lng))
	{
		if ($geo_lng < -180 || $geo_lng > 180) 
		{ 
			$geo_lng = $dlugosc_geograficzna;
		} 
	}
	else 
	{ 
		$geo_lng = $dlugosc_geograficzna;
	} 
	
	// Długość geograficzna: od -180 do 180 stopni 
	$geo_lat = intval($_COOKIE['geo_lat']); 
	// Dodatkowy safecheck
	if (is_numeric($geo_lat))
	{ 
		if ($geo_lat < 0 || $geo_lat > 90) 
		{ 
			$geo_lat = $szerokosc_geograficzna;
		} 
	} 
	else 
	{ 
		$geo_lat = $szerokosc_geograficzna;
	}
}
?>