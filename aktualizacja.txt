Je�li aktualizujesz FLEKS ze starszej wersji* do aktualnej (2.2):

1. Wypakuj archiwum na dysk, otw�rz folder "fleks" NA DYSKU
2. Usu� plik dane.php 
3. Otw�rz folder "css"
4. Usu� nast�puj�ce pliki: dzien.css, noc.css, pc.css
5. Je�li modyfikowa�e� plik "mobi.css" na serwerze - usu� go z dysku (nie serwera!)
6. Utw�rz kopi� plik�w na serwerze 
7. Wy�lij pliki na serwer, nadpisuj�c stare
8. Zapisz, gotowe. ;)

====================

Je�li aktualizujesz z wersji BETA 2 lub 3:
9. Otw�rz dane.php na serwerze i dodaj ten kod przed opcjami u�ytkownika:
//Zmienia styl ustawie� na taki sam jak ten u�yty na forum
//Zmie� na TRUE by w��czy�
$jeden_styl = FALSE;

====================

*UWAGA!
Je�li posiadasz wersj� BETA 1 najpierw przeprowad� aktualizacj� do wersji BETA 2:
http://sourceforge.net/projects/jdevo/files/FLEKS%20%28beta%29/fleks_beta2.zip/download