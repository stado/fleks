<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
require('dane.php');

// Zliczamy ilość opcji
$ilosc_opcji = count($opcja);	

// Pętla opcji		
for($i=0;$i < $ilosc_opcji; $i++)		
{	
	// Zwraca tablicę
	$aktualna_opcja	 = ${$opcja[$i]};
	// Zwraca string
	$nazwa_opcji	 = $opcja[$i];
	// Zwraca liczbę
	$ilosc_wartosci  = count(array_keys($aktualna_opcja));
	
	if (isset($_POST["$nazwa_opcji"]))	
	{
		// Pętla wartości
		for($x=0;$x < $ilosc_wartosci; $x++)
		{
			// Jeśli wartość domyślna
			if ($_POST["$nazwa_opcji"] == "0")
			{
				// Usuwa cookie
				setCookie("$nazwa_opcji", "", time() - 3600);
			}
			
			// Jeśli parametr ma daną wartość
			elseif ($_POST["$nazwa_opcji"] == "$x")
			{
				// Tworzy cookie
				setCookie("$nazwa_opcji", "$x", time()+24*3600*365);
			}
		}
	}
}

// Pobieranie współrzędnych geograficznych
if (isset($_POST["geo_lng"]) && isset($_POST["geo_lat"]))
{
	//Zmienia typ danych na liczbę całkowitą
	$geo_lng = intval($_POST['geo_lng']);
	$geo_lat = intval($_POST['geo_lat']);

	setCookie("geo_lng", "$geo_lng", time()+24*3600*365);
	setCookie("geo_lat", "$geo_lat", time()+24*3600*365);
}

// Tworzy cookie z datą ostatniej modyfikacji ustawień
setCookie ("timestamp", time());

// Przekierowuje do strony ustawień
header('Location: ustawienia.php?ok');
?>