<?php
// Baza danych
require('dane.php');
// Wyświetla błędy tylko jeśli jest włączony tryb debugowania
error_reporting($debug ? E_ERROR | E_WARNING | E_PARSE : 0);
// Zmienne używane w różnych plikach
require('globalne.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title><?= $FLEKS['TYTUL'] ?></title>
	<link href="favicon.png" rel="icon" type="image/x-icon" />
	<link rel="stylesheet" href="css/fleks.css" />
	<?php echo ($jeden_styl ? '<link rel="stylesheet" href="styl.css?'.time().'" />'."\n" : ''); ?>
	<script type="text/javascript">
	// W przyszłości zostawić tylko confirm, kod resetujący przenieść do php
	function fleks_reset()
	{
		if(confirm("Przywrócić ustawienia domyślne?"))
		{
			var inputs = document.getElementById("fleks_options");
			for (var i=0; i<inputs.length; i++)
			{
				if (inputs.elements[i].value == 0)
				{
					inputs.elements[i].checked = true;
				}
			}
			document.getElementById('geo_lat').value = <?=$szerokosc_geograficzna?>;
			document.getElementById('geo_lng').value = <?=$dlugosc_geograficzna?>;
		}
	}
	</script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
</head>
<body>
<div id="fleks_main" class="pun">
<header>
	<h1><?= $FLEKS['TYTUL'] ?></h1>
	<h2><?= $FLEKS['PODTYTUL'] ?></h2>
	<?php 
	if(isset($_GET['ok']))
	{
		echo '<h3>Zapisano ustawienia. Odśwież forum kombinacją klawiszy [Ctrl] + [F5] by zobaczyć zmiany.</h3>';
	}
	?>
	<?php if ($debug == TRUE): ?>
	<h4><a href="styl.css">Link do wygenerowanego stylu</a></h4>
	<?php endif; ?>
</header>
<form action="zapisz_2.php" method="post" id="fleks_options">
<?php
foreach ($opcja as $i => $nazwa_opcji)
{	
	// Zwraca zmienną (tablicę)
	$aktualna_opcja	 = ${$opcja[$i]};
	// Zwraca liczbę
	$ilosc_wartosci  = count(array_keys($aktualna_opcja));

	echo '<div class="radio '.$nazwa_opcji.'">';
	if(isset($tytul_opcji[$nazwa_opcji])){
	echo '<h3>'.$tytul_opcji[$nazwa_opcji].'</h3>'."\n";
	}
	if(isset($opis_opcji[$nazwa_opcji])){
	echo '<h4>'.$opis_opcji[$nazwa_opcji].'</h4>'."\n";
	}
	//echo 'Ta opcja ma '.$ilosc_wartosci.' wartości: ';
	
	// Pętla wartości
	foreach ($aktualna_opcja as $x => $aktualna_wartosc)
	{
		// Jeśli istnieje cookie, lub nie istnieje i wartość domyślna - dodaje wartośc checked
		$checked = ($_COOKIE[$nazwa_opcji] == "$x" || !isset($_COOKIE[$nazwa_opcji]) && $x==0) ? ' checked' : '';
		
		// Jeśli istnieje opis to tworzy tytuł inputa
		$title = (isset($aktualna_wartosc['OPIS'])) ? ' title="'.$aktualna_wartosc['OPIS'].'"' : '';	
		
		// Kod inputa
		echo '<label'.$title.'>';
		//($ilosc_wartosci == 1) ? $typ = 'checkbox' :  $typ = 'radio'; 
		//echo '<input type="'.$typ.'" name="'.$nazwa_opcji.'" value="'.$x.'"'.$checked.'>';
		echo '<input type="radio" name="'.$nazwa_opcji.'" value="'.$x.'"'.$checked.'>';
		echo $aktualna_wartosc['NAZWA'].'</label>';
	}
	echo '</div>';
	echo "\n";
}
?>
<div style="width: 100%; margin: auto;">
	<h3>Twoja lokalizacja</h3>
	<h4>Jeśli mieszkasz w dużej odległości od domyślnej lokalizacji (za granicą) zaleca się jej zmianę na poprawną.</h4>
	<div id="mapdiv" style="width:100%; height: 300px; border-radius: 6px;"></div>
		<h4>Współrzędne geograficzne (w stopniach):</h4>
		<input type="text" id="geo_lat" name="geo_lat" size="1" maxlength="4" value="<?=$geo_lat?>">Szerokość		<input type="text" id="geo_lng" name="geo_lng" size="1" maxlength="4" value="<?=$geo_lng?>">Długość
	</div>
<br/>
<script type="text/javascript">
var map;
function mapa()
{
	// Tworzy mapę, centrując ją na danych koordynatach
	var myLatlng = new google.maps.LatLng(<?=$geo_lat?>,<?=$geo_lng?>);
	var opts = {'center': myLatlng, 'zoom':3, 'mapTypeId': google.maps.MapTypeId.ROADMAP }
	map = new google.maps.Map(document.getElementById('mapdiv'),opts);

	// Tworzy marker na powyższych koordynatach
	var marker = new google.maps.Marker({
	position: myLatlng,
	map: map,
	title: 'Twoja lokalizacja'
	});
	
	// Na kliknięcie uzupełnia inputy i przesuwa marker
	google.maps.event.addListener(map,'click',function(event) {
		document.getElementById('geo_lat').value = Math.round(event.latLng.lat())
		document.getElementById('geo_lng').value = Math.round(event.latLng.lng())
		marker.setPosition(event.latLng);
	})
}

window.onload = mapa
</script>
<div id="fleks_buttons">
	<input type="submit" value="Zapisz ustawienia" id="zapisz">
	<input type="submit" value="Reset ustawień" title="Ustawia wartości domyslne" onclick="fleks_reset();">
</div>
</form>
</div>
<footer>
	<a href="https://sourceforge.net/projects/jdevo/">FLEKS</a> <?=$wersja?> © 2012-2014
	<a href="http://pridelands.eu/">Lewek</a>.
	<br>
	Styl © <?=date("Y")?> <a href="<?=$FLEKS['ADRES_FORUM']?>"><?=$FLEKS['NAZWA_FORUM']?></a>.
</footer>
</body>
</html>