﻿<?php
// Dane strony 
// Uważaj na akapity, pamiętaj że muszą się domykać!

// Tytuł strony ustawień. Możesz tu użyć HTML
$FLEKS['TYTUL'] = 'FLEKS - ustawienia forum';
// Podtytuł strony ustawień. Możesz tu użyć HTML
$FLEKS['PODTYTUL'] = 'Tutaj możesz zmienić ustawienia swojego forum.';
// Nazwa twojego forum
$FLEKS['NAZWA_FORUM'] = 'Moje Forum';
// Adres twojego forum
$FLEKS['ADRES_FORUM'] = 'http://example.com/';

// Dane do wykrywania pory dnia
$strefa_czasowa = 'Europe/Warsaw'; 
// Współrzędne geograficzne w stopniach
// Domyślne (52,19) wskazują w przybliżeniu na środek Polski
$szerokosc_geograficzna  = 52;
$dlugosc_geograficzna = 19;

// Tryb debugowania
// Gdy jest aktywny w style.css wyświetlą się dodatkowe dane
// Oraz zostanie wyłączona minifikacja kodu
// Zmień na TRUE by włączyć
$debug = FALSE;

// Zmienia styl ustawień na taki sam jak ten użyty na forum
// Zmień na TRUE by włączyć
$jeden_styl = FALSE;

// ------------------
// Opcje użytkownika:
// ------------------

// Zmiana stylu zależna od pory dnia
$opcja[] = 'styl';
$tytul_opcji['styl'] = 'Styl dzienny/nocny';
$opis_opcji['styl'] = 'Zmienia styl zależnie od pory dnia.';

$styl[0]['NAZWA'] = 'Automatycznie';
$styl[0]['OPIS'] = 'Zmienia styl zależnie od godziny';
$styl[0]['KOD'] = '/* Zmiana stylu automatyczna */';

$styl[1]['NAZWA'] = 'Dzienny';
$styl[1]['OPIS'] = 'Zawsze styl dzienny';
$styl[1]['KOD'] = '/* #tylkodzienny */';
$styl[1]['PLIK'] = 'css/dzien.css';

$styl[2]['NAZWA'] = 'Nocny';
$styl[2]['OPIS'] = 'Zawsze styl dzienny';
$styl[2]['KOD'] = '/* #tylkonocny */';
$styl[2]['PLIK'] = 'css/noc.css';

$styl[3]['NAZWA'] = 'Wyłącz styl';
$styl[3]['OPIS'] = 'Wyłącza styl.';
$styl[3]['KOD'] = '/* styl wyłączony */';

// Szerokość forum
$opcja[] = 'width';
$tytul_opcji['width'] = 'Szerokość forum';

$width[0]['NAZWA'] = 'Domyślnie';
$width[0]['KOD'] = '/* domyślnie */';

$width[1]['NAZWA'] = 'Pełna szerokość';
$width[1]['KOD'] = '#punwrap {width: 98%;}';
	
$width[2]['NAZWA'] = 'Letterbox';
$width[2]['OPIS'] = 'Wąska szerokość forum.';
$width[2]['KOD'] = '#punwrap {width: 75% !important; min-width:900px; max-width:1300px; margin: 50px auto;}';


// Urządzenie
// Zmień wartość na FALSE by wyłączyć automatyczne wykrywanie urządzenia.
$WERJSA_MOBILNA = TRUE;
$opcja[] = 'urzadzenie';
$tytul_opcji['urzadzenie'] = 'Wersja forum';

$urzadzenie[0]['NAZWA'] = 'Wykryj automatycznie';
$urzadzenie[0]['KOD'] = '/* automatycznie */';

$urzadzenie[1]['NAZWA'] = 'Na komputery';
$urzadzenie[1]['KOD'] = '/* pc */';

$urzadzenie[2]['NAZWA'] = 'Mobilna';
$urzadzenie[2]['KOD'] = '/* mobi */';

?>