<?php
// Baza danych
require_once ('dane.php');
// Wyświetla błędy tylko jeśli jest włączony tryb debugowania
error_reporting($debug ? E_ERROR | E_WARNING | E_PARSE : 0);
// Zmienne używane w różnych plikach
require_once ('globalne.php');
// Skrypt od pory dnia
require_once ('sunrise-sunset.php');

// Zmienna z czasem ostatniej modyfikacji ustawień (z cookie)
$czas_zmiany_ustawien = (isset($_COOKIE['timestamp'])) ? intval($_COOKIE['timestamp']) : 0;

// Zmienne z czasem wschodu i zachodu słońca
$zachod_dzis = 		date ('U', strtotime(date("Y-m-d") . ' '. $sunset));
$wschod_dzis = 		date ('U', strtotime(date("Y-m-d") . ' '. $sunrise));
$zachod_wczoraj =	date ('U', strtotime(date("Y-m-d", $yesterday) . ' '. $sunset_yesterday));
$wschod_jutro = 	date ('U', strtotime(date("Y-m-d", $tommorow) . ' '. $sunrise_tommorow));

// Jeśli mamy czas przed zachodem Słońca to ostatni zachód był wczoraj. Jeśli po - dzisiaj.
$ostatni_zachod = (time() < $zachod_dzis) ? $zachod_wczoraj : $zachod_dzis;	
// Jeśli mamy czas po wschodzie Słońca to następny wschód będzie jutro. Jeśli przed - dziś.
$nastepny_wschod = (time() > $wschod_dzis) ? $wschod_jutro : $wschod_dzis;

// Czas ostatniego przełączenia stylu między nocnym a dziennym.
// Jeśli dzień - styl zmienił się dziś ze wschodem Słońca. W przeciwnym razie 
$czas_zmiany_stylu = ($dayornight == 'day') ? $wschod_dzis : $ostatni_zachod;
// Jeśli dzień - styl wygasa dziś z zachodem Słońca.
$czas_wygasniecia  = ($dayornight == 'day') ? $zachod_dzis : $nastepny_wschod;	

// Czasem ostatniej modyfikacji pliku
$czas_modyfikacji = ($czas_zmiany_stylu > $czas_zmiany_ustawien) ? $czas_zmiany_stylu : $czas_zmiany_ustawien;

/*
Etag - przeglądarka zapisuje go przy pobieraniu pliku.
Przy każdym kolejnym zapytaniu porównuje etag w pamięci podręcznej z tym wysłanym przez serwer.
Jeśli etag zmienił się - przeglądarka może pobrać plik na nowo.
W praktyce przeglądarki przy cache'owaniu plików kierują się raczej innymi nagłówkami.
Jednakże nic nie szkodzi by go użyć. ;)
Przykładowy ETag: d150002
W przyszłych wersjach - rozszerzyć o współrzędne!
*/

// Dzień / noc
$etag = ($dayornight == 'day') ? 'd' :  'n';

// Tydzień w roku.
$etag = $etag.date('W');

// Zliczamy ilość opcji
$ilosc_opcji = count($opcja);

//Pętla opcji
foreach ($opcja as $i => $nazwa_opcji)
{	
	// Zwraca zmienną (tablicę)
	$aktualna_opcja	 = ${$opcja[$i]};
	// Zwraca liczbę
	$ilosc_wartosci  = count(array_keys($aktualna_opcja));
	
	// Wartość domyślna
	if (!isset($_COOKIE["$nazwa_opcji"]))
	{
		$etag = $etag.'0';
	}
	else
	{
		// Pętla wartości
		foreach ($aktualna_opcja as $x => $aktualna_wartosc)
		{
				// Jeśli cookie posiada daną wartość
				if ($_COOKIE["$nazwa_opcji"] == "$x")
				{
					$etag = $etag.$x;
				}
		}
	}
}
 
// Nagłówek pliku
header ('Content-type: text/css; charset=UTF-8');
header ('Last-Modified: ' . gmdate("D, d M Y H:i:s", $czas_modyfikacji) . ' GMT');
header ('Expires: ' . gmdate("D, d M Y H:i:s", $czas_wygasniecia) . ' GMT');
// "W/" oznacza weak. Dwa pliki o tym samym ETagu typu weak nie muszą być identyczne co do znaku.
// Jednakże dwa różne ETagi weak oznaczają różnicę w plikach.
header ('ETag: W/"'.$etag.'"');
header ("Cache-Control:private, must-revalidate");
header ('Connection: close');

// Dane trybu debugowania
if($debug == TRUE) {
	header ('Cache-Control: max-age=0');
	echo ('/* Last-Modified: ' . gmdate("D, d M Y H:i:s", $czas_modyfikacji) . ' GMT */'."\n");
	echo ('/* Expires: ' . gmdate("D, d M Y H:i:s", $czas_wygasniecia) . ' GMT */'."\n");
	echo ('/* ETag: W/"'.$etag.'" */'."\n");
}

// Kod do wykrywania urządzeń mobilnych - jeśli włączony
if($WERJSA_MOBILNA)
{
	require_once ('Mobile_Detect.php');	
	// Wykonaj kod wykrywający typ urządzenia
	$detect = new Mobile_Detect();
}
// Komentarz wstawiony przed minifikacją, by zawsze był widoczny.
echo '/* Styl wygenerowany przez FLEKS '.$wersja.' */'."\n";

//Minifikacja kodu jeśli nie ma trybu debugowania
if($debug != TRUE) {
	ob_start("compress");
}
//Funkcja minifikująca
function compress($buffer) {
	// Usunięcie komentarzy
	$buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
	// Usunięcie tabulatorów, spacji, nowych linii itp.
	$buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);
	return $buffer;
}

// Sprawdza która wersja stylu jest wybrana
// Jeśli ustawiono "wyłącz styl"
if ($_COOKIE['styl'] == '3')
{
	$wybrane_urzadzenie = FALSE;
}
// Jeśli nie wykryto urządzenia mobilnego i nie ustawiono ręcznie urządzenia
// Lub jeśli ustawiono urządzenie na PC
// Lub jeśli w dane.php wyłączono automatyczne wykrywanie
elseif (!$detect->isMobile() && !isset($_COOKIE['urzadzenie']) ||
		$_COOKIE['urzadzenie'] == '1' || $WERJSA_MOBILNA == FALSE)
{
	$wybrane_urzadzenie = 'pc';
}
else
{
	$wybrane_urzadzenie = 'mobi';
}

// Załącz plik właściwy dla danego urządzenia. 
if ($wybrane_urzadzenie == 'pc')
{
	print file_get_contents ('css/pc.css');
}
elseif ($wybrane_urzadzenie == 'mobi')
{
	print file_get_contents ('css/mobi.css');
}

// Sprawdza która wersja stylu jest wybrana
if (isset($_COOKIE['styl']))
{
	// Ręczny wybór stylu
	$wybrany_styl = 'recznie';
}
elseif ($dayornight != 'night')
{
	// Wersja komputerowa
	$wybrany_styl = 'dzien';
}
else
{
	// Wersja mobilna
	$wybrany_styl = 'noc';		
}

// Załącz plik danego stylu
if ($wybrany_styl == 'dzien')
{
	print file_get_contents ('css/dzien.css');
}
elseif ($wybrany_styl == 'noc')
{  
	print file_get_contents ('css/noc.css');
}

// Zliczamy ilość opcji
$ilosc_opcji = count($opcja);
// Główna pętla
for($i=0;$i < $ilosc_opcji; $i++)
{	
	// Zwraca tablicę
	$aktualna_opcja	 = ${$opcja[$i]};
	// Zwraca string
	$nazwa_opcji	 = $opcja[$i];
	// Zwraca liczbę
	$ilosc_wartosci  = count(array_keys($aktualna_opcja));
	
	// Wyświetla tytuł opcji w kodzie css
	echo "/* - ".$tytul_opcji[$nazwa_opcji]." - */ \n";
	
	// Jeśli nie ma cookie
	if (!isset($_COOKIE[$nazwa_opcji]))
	{
		// Wyświetla kod CSS opcji domyślnej
		echo $aktualna_opcja[0]['KOD'];
		
		// Dodaje plik opcji domyślnej, jeśli plik istnieje
		if(file_exists($aktulna_opcja[0]['PLIK']))
		{
			print file_get_contents ($aktualna_opcja[0]['PLIK']);
		}
	}
	// Jeśli istnieje cookie
	else
	{
		// Pętla wartości
		for($x=0;$x < $ilosc_wartosci; $x++)
		{
			// Jeśli istnieje cookie o danej wartości
			if ($_COOKIE[$nazwa_opcji] == "$x")
			{
				// Wyświetla kod CSS
				echo $aktualna_opcja[$x]['KOD'];
				// Dodaje plik, jeśli plik istnieje
				if(file_exists($aktualna_opcja[$x]['PLIK']))
				{
					print file_get_contents ($aktualna_opcja[$x]['PLIK']);
				}
			}
		}
	}
	echo "\n\n";
}

// Naprawa PBB ChatBox w Operze Mini
if ($detect->version('Opera Mini'))
{
	echo "\n body div#punwrap div.pun div#chatbox {height: 100% !important;} \n";
}

// Koniec minifikacji
if($debug != TRUE)
{
	ob_end_flush();
}
?>