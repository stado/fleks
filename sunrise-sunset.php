<?php
/*
Sunrise - Sunset PHP Script by Mike Challis
Free PHP Scripts - www.642weather.com/weather/scripts.php
Download         - www.642weather.com/weather/scripts/sunrise-sunset.zip
Live Example     - www.642weather.com/weather/sunrise-sunset.php
Contact Mike     - www.642weather.com/weather/contact_us.php
Donate: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=2319642

Modified for FLEKS by Lewek. 
Based on version: 1.06 (18-Jul-2009)

**Requires PHP 5**
(PHP version 5.1.3 or higher is recommended due to date_sunset bugs in prior versions)

You are free to use and modify the code

This php code provided "as is", and Long Beach Weather (Michael Challis)
disclaims any and all warranties, whether express or implied, including
(without limitation) any implied warranties of merchantability or
fitness for a particular purpose.

*/
// Zmienne do dodawania/odejmowania jednej doby przy dacie
$yesterday = time() - (24 * 60 * 60);
$tommorow  = time() + (24 * 60 * 60);

// Format daty używany w trybie Debug
$time_format = 'Y-m-d H:i';
// http://en.wikipedia.org/wiki/Twilight
$zenith = 90+(50/60); // True sunrise/sunset

// Set timezone in PHP5/PHP4 manner
if (!function_exists('date_default_timezone_set')) {
        putenv("TZ=" . $strefa_czasowa);
} else {
        date_default_timezone_set("$strefa_czasowa");
}

// find time offset in hours
$tzoffset = date("Z")/60 / 60;

// determine if it is 'night' or 'day'
$dayornight = day_or_night();

$sunset_yesterday = date_sunset (strtotime("-1 day"), SUNFUNCS_RET_STRING, $geo_lat, $geo_lng, $zenith, $tzoffset);
$sunrise =	date_sunrise(strtotime("now"), SUNFUNCS_RET_STRING, $geo_lat, $geo_lng, $zenith, $tzoffset);
$sunset = 	date_sunset (strtotime("now"), SUNFUNCS_RET_STRING, $geo_lat, $geo_lng, $zenith, $tzoffset);
$sunrise_tommorow =	date_sunrise(strtotime("+1 day"), SUNFUNCS_RET_STRING, $geo_lat, $geo_lng, $zenith, $tzoffset);

$sunset_time_yesterday =	date($time_format, strtotime(date("Y-m-d", $yesterday) . ' '. $sunset_yesterday));
$sunrise_time =				date($time_format, strtotime(date("Y-m-d") . ' '. $sunrise));
$sunset_time =				date($time_format, strtotime(date("Y-m-d") . ' '. $sunset));
$sunrise_time_tommorow =	date($time_format, strtotime(date("Y-m-d", $tommorow) . ' '. $sunrise_tommorow));


// print debugging info, settings and variables for testing
if ($debug) {
  echo '/*' . "\n" . 'FLEKS - ustawienia i informacje:'."\n";
  echo 'TRYB DEBUGOWANIA - WYŁĄCZYĆ PO ZAKOŃCZENIU PRAC!'."\n";

  echo 'Wersja PHP: '.phpversion()."\n";
  echo date("d M Y H:i:s")." (czas na serwerze)\n";
  echo gmdate("d M Y H:i:s")." (czas GTM)\n\n";

  echo 'Strefa czasowa = '.$strefa_czasowa.' (UTC '.$tzoffset.")\n";
  echo 'Szerokośc geograficzna = '.$geo_lat."\n";
  echo 'Długość geograficzna = '.$geo_lng."\n";
  echo 'Zenit = ' . $zenith . "\n\n";

  echo 'Pora dnia: '.$dayornight."\n";
  echo 'Wschód słońca: '.$sunrise_time."\n";
  echo 'Zachód słońca: '.$sunset_time."\n";
  echo 'Wczorajszy zachód słońca: '.$sunset_time_yesterday."\n";
  echo 'Jutrzejszy wschód słońca: '. $sunrise_time_tommorow."\n";

  echo '*/'."\n\n";
}

function day_or_night() {
   // determine if it is 'night' or 'day'
   // returns string: 'night' or 'day'
   global $geo_lat, $geo_lng, $tzoffset, $zenith;

   $sunrise_epoch = date_sunrise(time(), SUNFUNCS_RET_TIMESTAMP, $geo_lat, $geo_lng, $zenith, $tzoffset);
   $sunset_epoch  = date_sunset(time(), SUNFUNCS_RET_TIMESTAMP, $geo_lat, $geo_lng, $zenith, $tzoffset);
   $time_epoch = time(); // time now

   if ($time_epoch >= $sunset_epoch or $time_epoch <= $sunrise_epoch) {
          return 'night';
   } else {
          return 'day';
   }

}
?>